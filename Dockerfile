FROM alpine:latest

RUN apk update && apk add wget ca-certificates && \

RUN adduser -S -H -s /bin/sh www
RUN chmod 0755 /sbin/sreracha

EXPOSE 8080

USER www

# Configure sreracha to run on a non-default port - needed when running as
# a non-root user.
ENV PORT=8080
ENV REDIS_URL=redis://redis:6379

ENTRYPOINT /sbin/sreracha
